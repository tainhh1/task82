package com.devcamp.pizza365.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.pizza365.entity.Order;

public class OrderExcelExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Order> orders;

	

	public OrderExcelExporter(List<Order> orders) {
		super();
		this.orders = orders;
		this.workbook = new XSSFWorkbook();
	}

	/**
	 * Tạo các ô cho excel file.
	 * 
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof String) {
			cell.setCellValue((String) value);
		} else if (value instanceof Date) {
			cell.setCellValue(dateFormater.format(value));
		}
		cell.setCellStyle(style);
	}

	/**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
//		đầu tiên tạo ra 1 sheet
		sheet = workbook.createSheet("Orders");
//		sheet sẽ  tạo ra 1 row tương ứng hàng đầu tiên
		Row row = sheet.createRow(0);

//		set font cho header
		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		style.setFont(font);
		font.setBold(true);
		font.setFontHeight(14);
//		tạo từng cell để thêm dữ  liệu
//		int columnCount = 0;
		createCell(row, 0, "Order Id", style);
		createCell(row, 1, "Order Date", style);
		createCell(row, 2, "Required Date", style);
		createCell(row, 3, "Shipped Date", style);
		createCell(row, 4, "Status", style);
		createCell(row, 5, "Comments", style);
		createCell(row, 6, "Customer Id", style);
	}

	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLine() {
		int rowCount = 1;
		CellStyle style = workbook.createCellStyle();

		XSSFFont font = workbook.createFont();
		font.setFontHeight(11);
		style.setFont(font);

		for (Order order : this.orders) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			createCell(row, columnCount++, order.getId(), style);
			createCell(row, columnCount++, order.getOrderDate(), style);
			createCell(row, columnCount++, order.getRequiredDate(), style);
			createCell(row, columnCount++, order.getShippedDate(), style);
			createCell(row, columnCount++, order.getStatus(), style);
			createCell(row, columnCount++, order.getComments(), style);
			createCell(row, columnCount++, order.getCustomer().getId(), style);
		}
	}

	/**
	 * xuất dữ liệu ra dạng file
	 * 
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLine();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();
	}

}
