package com.devcamp.pizza365.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.OrderRepository;
import com.devcamp.pizza365.service.OrderExcelExporter;

@CrossOrigin
@RestController
public class OrderController {
	@Autowired
	OrderRepository orderRepository;
	
	@GetMapping("/orders/excel/export")
	public void exportOrderToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormater.format(new Date());
		
		String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=orders_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        
        List<Order> orders = new ArrayList<Order>();
        orderRepository.findAll().forEach(orders::add);
        OrderExcelExporter excelExporter = new OrderExcelExporter(orders);
        excelExporter.export(response);
	}
}
