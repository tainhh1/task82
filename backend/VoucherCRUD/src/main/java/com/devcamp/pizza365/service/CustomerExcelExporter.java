package com.devcamp.pizza365.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.pizza365.entity.Customer;

public class CustomerExcelExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Customer> customers;

	public CustomerExcelExporter(List<Customer> customers) {
		super();
		this.customers = customers;
		this.workbook = new XSSFWorkbook();
	}
	
	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof String) {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}
	
	public void writeHeaderLine() {
		sheet = workbook.createSheet("Customers");
		Row row = sheet.createRow(0);
		
		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		style.setFont(font);
		font.setBold(true);
		font.setFontHeight(14);
		
		int rowCount = 0;
		
		createCell(row, rowCount++, "Customer Id", style);
		createCell(row, rowCount++, "Customer Name", style);
		createCell(row, rowCount++, "Customer Phone", style);
		createCell(row, rowCount++, "Customer Address", style);
		createCell(row, rowCount++, "Customer City", style);
		createCell(row, rowCount++, "Customer State", style);
		createCell(row, rowCount++, "Customer Postal Code", style);
		createCell(row, rowCount++, "Customer Country", style);
		createCell(row, rowCount++, "Customer Sale rep Employee Number", style);
		createCell(row, rowCount++, "Customer Credit limit", style);
	}
	
	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLine() {
		int rowCount = 1;
		CellStyle style = workbook.createCellStyle();

		XSSFFont font = workbook.createFont();
		font.setFontHeight(11);
		style.setFont(font);

		for (Customer customer : customers) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			
			createCell(row, columnCount++ , customer.getId(), style);
			createCell(row, columnCount++ , customer.getFirstName() + " " + customer.getLastName(), style);
			createCell(row, columnCount++ , customer.getPhoneNumber(), style);
			createCell(row, columnCount++ , customer.getAddress(), style);
			createCell(row, columnCount++ , customer.getCity(), style);
			createCell(row, columnCount++ , customer.getState(), style);
			createCell(row, columnCount++ , customer.getPostalCode(), style);
			createCell(row, columnCount++ , customer.getCountry(), style);
			createCell(row, columnCount++ , customer.getSalesRepEmployeeNumber(), style);
			createCell(row, columnCount++ , customer.getCreditLimit(), style);
		}
	}
	
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLine();
		
		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();
		
		outputStream.close();
	}


}
