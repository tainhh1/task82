package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.query.GetCountryCustomer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	@Query(value = "select c.country , count(c.id) numberCustomer\r\n"
			+ "from customers c \r\n"
			+ "group by c.country ", nativeQuery = true)
	List<GetCountryCustomer> getNumberCustomerByCountry(); 
}
