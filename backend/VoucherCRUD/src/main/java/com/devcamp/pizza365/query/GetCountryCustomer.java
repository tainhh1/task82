package com.devcamp.pizza365.query;

public interface GetCountryCustomer {
	public String getCountry();
	public long getNumberCustomer();
}
