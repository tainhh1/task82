$(document).ready(() => {
    let gCustomerTable = $('#customer-table').DataTable({
        responsive: true,
        lengthChange: false,
        autoWidth: false,
        columns: [
            { data: 'firstName' },
            { data: 'lastName' },
            { data: 'phoneNumber' },
            { data: 'address' },
            { data: 'city' },
            { data: 'state' },
            { data: 'postalCode' },
            { data: 'country' },
            { data: 'salesRepEmployeeNumber' },
            { data: 'creditLimit' },
        ],
    });

    function loadTable(paramCustomer) {
        gCustomerTable.clear();
        gCustomerTable.rows.add(paramCustomer);
        gCustomerTable.draw();
    }

    $.get('http://localhost:8080/api/allCustomers', loadTable);

    $('#btn-export-table').click(() => {
        $('#customer-table').excelexportjs({
            containerid: 'customer-table',
            datatype: 'table',
            worksheetName: 'Customer',
        });
    });

    $('#btn-get-excel').click(() => {
        window.location.href = 'http://localhost:8080/api/customer/excel/export';
    });
});
