$(document).ready(() => {
    let gCustomerTable = $('#customer-table').DataTable({
        responsive: true,
        lengthChange: false,
        autoWidth: false,
        buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
        columns: [
            { data: 'firstName' },
            { data: 'lastName' },
            { data: 'phoneNumber' },
            { data: 'address' },
            { data: 'city' },
            { data: 'state' },
            { data: 'postalCode' },
            { data: 'country' },
            { data: 'salesRepEmployeeNumber' },
            { data: 'creditLimit' },
        ],
    });

    function loadTable(paramCustomer) {
        gCustomerTable.clear();
        gCustomerTable.rows.add(paramCustomer);
        gCustomerTable.draw();
        gCustomerTable.buttons().container().appendTo('#customer-table_wrapper .col-md-6:eq(0)');
    }

    $.get('http://localhost:8080/api/allCustomers', loadTable);
});
